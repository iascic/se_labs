def helpFunc():
	print('''Here's how it works:
	
When it is your turn to play, you need to specify which square 
you want your "X" or "O" to go in. You do this 
by typing the ROW 'space' COLUMN you want. 
For example, if you want to play your X like this:
	
 --- --- ---
|   |   | X |
 --- --- ---
|   |   |   |
 --- --- ---
|   |   |   | 
 --- --- --- 
	 
That would be the first row, and third column. 
So you would type "1 3". In the same way, 
the center is row 2, column 2, so you would type "2 2".Got it? 
Don't got it? Whatever, time to play!\n\n''')
	input("Press ENTER to continue:")

def greeting():
	print("Welcome to the Tic-Tac-Toe Game!\n\n")
	print("Type 'help' for the rules, or press ENTER to begin...\n\n")
	askHelp = input(">>>")
	if askHelp == "help":
		helpFunc()
	print("\nOkay, then! Let's begin!!\n")	
	 
def checkGame(game): #game is a 3x3 list of lists
	row1 = game[0]
	row2 = game[1]
	row3 = game[2]
	
	if row1[0] == row1[1] and row1[0] == row1[2]: # Checking rows first
		if row1[0] == 1:
			return "Player 1 wins!"
		elif row1[0] == 2:
			return "Player 2 wins!"
	if row2[0] == row2[1] and row2[0] == row2[2]:
		if row2[0] == 1:
			return "Player 1 wins!"
		elif row2[0] == 2:
			return "Player 2 wins!"
	if row3[0] == row3[1] and row3[0] == row3[2]:
		if row3[0] == 1:
			return "Player 1 wins!"
		elif row3[0] == 2:
			return "Player 2 wins!"
			
	if row1[0] == row2[0] and row1[0] == row3[0]: # Now checking columns
		if row1[0] == 1:
			return "Player 1 wins!"
		elif row1[0] == 2:
			return "Player 2 wins!"
	if row1[1] == row2[1] and row1[1] == row3[1]: 
		if row1[1] == 1:
			return "Player 1 wins!"
		elif row1[1] == 2:
			return "Player 2 wins!"
	if row1[2] == row2[2] and row1[2] == row3[2]: 
		if row1[2] == 1:
			return "Player 1 wins!"
		elif row1[2] == 2:
			return "Player 2 wins!"
			
	if row1[0] == row2[1] and row1[0] == row3[2]: #Finally, checking for diagonals
		if row1[0] == 1:
			return "Player 1 wins!"
		elif row1[0] == 2:
			return "Player 2 wins!"
	if row1[2] == row2[1] and row1[2] == row3[0]:
		if row1[2] == 1:
			return "Player 1 wins!"
		elif row1[2] == 2:
			return "Player 2 wins!"
	elif 0 not in row1 and 0 not in row2 and 0 not in row3:
			return "It's a draw!"
	else:
		return None
	
def printBoard(board): # board is a 3x3 list of lists, this function prints the current state of the board
	newBoard = [[],[],[]] #This first section converts the numbered lists to X's and O's
	index = 0
	for row in board:
		for element in row:
			if element == 0:
				newBoard[index].append(' ')
			elif element == 1:
				newBoard[index].append('X')
			elif element == 2:
				newBoard[index].append('O')
		index += 1
	dashes = ' ---'
	counter = 0
	print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n") #This looks weird but it spaces the redrawn board 
	#far enough down the terminal window (at least for me) that the game appears to "update" the board
	while counter < 3: #Now we will loop through and print the formatted board
		print(dashes * 3)
		print('| {} | {} | {} |'.format(newBoard[counter][0], newBoard[counter][1], newBoard[counter][2]))
		counter += 1
	print(dashes * 3)
	


def player1(): #Get player input, convert to list of numbers and return them
	player1Turn = input("\nYour turn Player 1:\n\n>>>").strip().split()
	intList = []
	for elem in player1Turn:
		intList.append(int(elem) - 1) #This converts player friendly input to correct list indexes for program
	return intList
			
	
def player2(): #Same as above
	player2Turn = input("\nYour turn Player 2:\n\n>>>").strip().split()
	intList = []
	for elem in player2Turn:
		intList.append(int(elem) - 1)
	return intList
	
def player1Turn(): #Makes sure play is in new spot, and kicks it back if spot is already taken
	user1Turn = player1()
	if board[user1Turn[0]][user1Turn[1]] == 0:
		board[user1Turn[0]][user1Turn[1]] = 1
		return False
	else:
		print("Sorry, that spot is taken. Try again.")
		return True
		
def player2Turn(): #This is almost unnecessary except for the fact that we are putting a 2 instead of a 1
	user2Turn = player2()
	if board[user2Turn[0]][user2Turn[1]] == 0:
		board[user2Turn[0]][user2Turn[1]] = 2
		return False
	else:
		print("Sorry, that spot is taken. Try again.")
		return True

board = [[0, 0, 0], # initializing game board as global variable
		 [0, 0, 0],
		 [0, 0, 0]]

def main(): #where the magic happens
	greeting()
		
	noWinner = True
	while noWinner: # begin game loop
		if noWinner == True: #player 1 turn loop
			turn = True
			while turn:
				printBoard(board)
				turn = player1Turn()
		if checkGame(board) != None and noWinner == True: #check for a winner
			printBoard(board)
			print("\n")
			print(checkGame(board))
			noWinner = False
		if noWinner == True:	#player 2 turn loop
			turn = True
			while turn:
				printBoard(board)	
				turn = player2Turn()
		if checkGame(board) != None and noWinner == True: #check for a winner
			printBoard(board)
			print("\n")
			print(checkGame(board))
			noWinner = False
		
	
if __name__ == "__main__": main()