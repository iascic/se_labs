
def open_files(archive_name):
    with open(archive_name, "r") as pn:
        number_in_list = pn.read().split()
        return number_in_list

def overlaping(list1,list2):
    new_list = []
    for i in list1:
        if i in list2:
            new_list.append(i)
    return new_list

if __name__ == "__main__":
    pn = open_files("primenumbers.txt")
    hn = open_files("happynumbers.txt")
    print(overlaping(pn,hn))